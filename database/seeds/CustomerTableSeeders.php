<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::add([
            'name' => 'Rafi Dzamara',
            'phone' => '085743568765',
            'address' => 'BTN Pukopad',
            'email' => 'rafidzamar98@gmail.com'
        ]);

        Customer::add([
            'name' => 'Ridan Ahmad',
            'phone' => '081265998098',
            'address' => 'Perum Cibinong Asri',
            'email' => 'ridan.ahmad026@gmail.com'
        ]);
    }
}